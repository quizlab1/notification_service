package config

import (
	"os"

	"github.com/spf13/cast"
)

const (
	// NewStatus enum
	NewStatus = "new"
	// SentStatus enum
	SentStatus = "sent"
	// FollowType enum
	FollowType = "follow"
	// PostType enum
	PostType = "post"
	// UnfollowType enum
	UnfollowType = "unfollow"
	// LikeType enum
	LikeType = "like"
	// JoinType enum
	JoinType = "join"
	// CommentType enum
	CommentType = "comment"
)

var (
	// TwilioFrom ...
	TwilioFrom = "+13218957953"
)

// Config ...
type Config struct {
	Environment      string // develop, staging, production
	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	LogLevel         string
	RPCPort          string
	APIKey           string
	// SentryDNS        string
	Notification struct {
		TwilioAccountSid string
		TwilioAuthToken  string
		TwilioFrom       string
	}

	Email struct {
		SMTPHost         string
		SMTPPort         int
		SMTPUser         string
		SMTPUserPass     string
		EmailFromHeader  string
	}
	UserServiceHost string
	UserServicePort int
}

// Load ...
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DB", "quizlabdb"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "1234"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.RPCPort = cast.ToString(getOrReturnDefault("RPC_PORT", ":9001"))
	c.APIKey = cast.ToString(getOrReturnDefault("API_KEY", "AAAAsnR2DCY:APA91bF_EKnuQKIj4ZTQjz53WKARp8Ce67fj9Fkn_HZ6Cw7BLcUvXX3jxLZuFV4dqEqJKwnJSg3o61Xbtpyl8eh9_as6NJqKfKOtR2IQjw1jqh0oq5u3MlhU08AxQsVxfTZyCIjj83rV"))
	// c.SentryDNS = cast.ToString(getOrReturnDefault("SENTRY_DNS", "https://df046c769b5a4be1a1a3d9f5cc773253@o384036.ingest.sentry.io/5214780"))

	c.Notification.TwilioAccountSid = cast.ToString(getOrReturnDefault("ACCOUNT_SID", "AC1e9a262afbc2daf25033e3ce1436e4b4"))
	c.Notification.TwilioAuthToken = cast.ToString(getOrReturnDefault("AUTH_TOKEN", "7fa4e8ecc3bab299dec71538086dfdae"))

	c.Email.SMTPHost = cast.ToString(getOrReturnDefault("SMTP_HOST", "smtp.gmail.com"))
	c.Email.SMTPPort = cast.ToInt(getOrReturnDefault("SMTP_PORT", 587))
	c.Email.SMTPUser = cast.ToString(getOrReturnDefault("SMTP_USER", "zafarfortest@gmail.com"))
	c.Email.SMTPUserPass = cast.ToString(getOrReturnDefault("SMTP_USER_PASSWORD", "facetapemail"))
	c.Email.EmailFromHeader = cast.ToString(getOrReturnDefault("EMAIL_FROM_HEADER", "zafarfortest@gmail.com"))

	c.UserServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST", "localhost"))
	c.UserServicePort = cast.ToInt(getOrReturnDefault("USER_SERVICE_PORT", 9000))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
