package main

import (
	"fmt"
	"net"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/quizlab/notification_service/config"
	"gitlab.com/quizlab/notification_service/deamons"
	"gitlab.com/quizlab/notification_service/pkg/logger"
	"gitlab.com/quizlab/notification_service/service"
	"github.com/sfreiberg/gotwilio"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	pb "gitlab.com/quizlab/notification_service/genproto/notification_service"
	"gitlab.com/quizlab/notification_service/service/grpcclient"
)

func main() {
	cfg := config.Load()

	twilio := gotwilio.NewTwilioClient(cfg.Notification.TwilioAccountSid, cfg.Notification.TwilioAuthToken)

	log := logger.New(cfg.LogLevel, "notification-service")
	defer logger.Cleanup(log)

	log.Info("main: pgxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresDatabase)

	connDb, err := sqlx.Connect("postgres", psqlString)

	if err != nil {
		log.Error("postgres connect error",
			logger.Error(err))
		return
	}

	client, err := grpcclient.New(cfg)

	if err != nil {
		log.Error("error while connecting other services")
		return
	}

	notificationService := service.NewNotificationService(connDb, log, client, twilio, cfg)

	lis, err := net.Listen("tcp", cfg.RPCPort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	// run push daemon
	pushDaemon := deamons.NewPushDaemon(log, cfg, client, notificationService)
	go pushDaemon.Init()

	// run email daemon
	emailDaemon := deamons.NewEmailDaemon(connDb, log, cfg)
	go emailDaemon.Init()

	if err != nil {
		log.Fatal("Cannot initialize sentry.io", logger.Error(err))
	}

	s := grpc.NewServer()

	pb.RegisterNotificationServiceServer(s, notificationService)

	reflection.Register(s)

	log.Info("main: server running",
		logger.String("port", cfg.RPCPort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
