 CREATE TABLE IF NOT EXISTS notif_statuses(
    id UUID PRIMARY KEY,
    name VARCHAR(20) NOT NULL CHECK (name=upper(name))
);

CREATE TABLE IF NOT EXISTS notif_types(
    id UUID PRIMARY KEY,
    name VARCHAR(20) NOT NULL CHECK (name=upper(name))
);

CREATE TABLE IF NOT EXISTS notifications(
    id UUID PRIMARY KEY,
    content TEXT NOT NULL ,
    sender_id UUID NOT NULL,
    sender_type VARCHAR(20) NOT NULL ,
    object_id UUID,
    object_type varchar(20)
);

CREATE TABLE IF NOT EXISTS notif_receivers(
    id UUID PRIMARY KEY,
    notif_id UUID NOT NULL REFERENCES notifications(id),
    receiver_id UUID NOT NULL,
    receiver_type VARCHAR(20) NOT NULL,
    notif_type_id UUID NOT NULL REFERENCES notif_types(id),
    notif_status_id UUID NOT NULL REFERENCES notif_statuses(id),
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    send_at TIMESTAMP
);

CREATE UNIQUE INDEX IF NOT EXISTS notif_receivers_i1  ON notif_receivers(
    notif_id, receiver_id, receiver_type
);


-- insert default types to notif_types table
INSERT INTO notif_types(id, name) VALUES('086032bc-fae4-4a85-aedd-83e37b52f309', 'FOLLOW') ON CONFLICT DO NOTHING;
INSERT INTO notif_types(id, name) VALUES('23f0ed87-6874-40be-993a-140fa31d7050', 'LIKE') ON CONFLICT DO NOTHING;
INSERT INTO notif_types(id, name) VALUES('70e21be4-d511-4284-b8e2-f1d94205ef1e', 'SYSTEM') ON CONFLICT DO NOTHING;


-- insert default values to notif_statuses table
INSERT INTO notif_statuses VALUES('2cb46139-7c5f-4e0d-88f6-50fe79986fa6', 'NEW') ON CONFLICT DO NOTHING;
INSERT INTO notif_statuses VALUES('f9c31d41-7e91-42a1-859b-e0991357566c', 'SENT') ON CONFLICT DO NOTHING;
INSERT INTO notif_statuses VALUES('303d5102-7595-4ba4-9c67-0c5d969d6a3e', 'READ') ON CONFLICT DO NOTHING;


-- migrating mail tables
 CREATE TABLE IF NOT EXISTS email_text (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    subject TEXT NOT NULL,
    body TEXT NOT NULL
 );

 CREATE TABLE IF NOT EXISTS email_send_email (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    email VARCHAR(100) NOT NULL,
    send_time TIMESTAMP,
    send_status BOOLEAN NOT NULL DEFAULT FALSE,
    text_id UUID NOT NULL REFERENCES email_text(id)
 );

