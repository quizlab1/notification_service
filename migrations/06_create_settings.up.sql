CREATE TABLE IF NOT EXISTS notif_settings(
    user_id UUID PRIMARY KEY,
    post BOOLEAN DEFAULT TRUE,
    follow BOOLEAN DEFAULT TRUE,
    direct_message BOOLEAN DEFAULT TRUE,
    email BOOLEAN DEFAULT TRUE
);