-- removing foreign keys from notif_receivers
ALTER TABLE notif_receivers DROP COLUMN notif_type;
ALTER TABLE notif_receivers DROP COLUMN notif_status;

-- 
DELETE FROM notif_types WHERE notif_type = 'follow';
DELETE FROM notif_types WHERE notif_type = 'like';
DELETE FROM notif_types WHERE notif_type = 'system';

-- insert default values to notif_statuses table
DELETE FROM notif_statuses WHERE notif_status = 'new';
DELETE FROM notif_statuses WHERE notif_status = 'sent';
DELETE FROM notif_statuses WHERE notif_status = 'read';

-- notif_statuses table alter statements
ALTER TABLE notif_statuses DROP COLUMN notif_status;
ALTER TABLE notif_statuses ADD COLUMN id UUID PRIMARY KEY;
ALTER TABLE notif_statuses ADD COLUMN name VARCHAR(20) CHECK (name=upper(name));
ALTER TABLE notif_statuses ALTER COLUMN name SET NOT NULL;


-- notif_types table alter statements
ALTER TABLE notif_types DROP COLUMN notif_type;
ALTER TABLE notif_types ADD COLUMN id UUID PRIMARY KEY;
ALTER TABLE notif_types ADD COLUMN name VARCHAR(20) CHECK (name=upper(name));
ALTER TABLE notif_types ALTER COLUMN name SET NOT NULL ;

-- notif_receivers alter statements
ALTER TABLE notif_receivers ADD COLUMN notif_type_id UUID NOT NULL REFERENCES notif_types(id);
ALTER TABLE notif_statuses ADD COLUMN notif_status_id UUID NOT NULL REFERENCES notif_statuses(id);

-- insert default types to notif_types table
INSERT INTO notif_types(id, name) VALUES('086032bc-fae4-4a85-aedd-83e37b52f309', 'FOLLOW') ON CONFLICT DO NOTHING;
INSERT INTO notif_types(id, name) VALUES('23f0ed87-6874-40be-993a-140fa31d7050', 'LIKE') ON CONFLICT DO NOTHING;
INSERT INTO notif_types(id, name) VALUES('70e21be4-d511-4284-b8e2-f1d94205ef1e', 'SYSTEM') ON CONFLICT DO NOTHING;


-- insert default values to notif_statuses table
INSERT INTO notif_statuses VALUES('2cb46139-7c5f-4e0d-88f6-50fe79986fa6', 'NEW') ON CONFLICT DO NOTHING;
INSERT INTO notif_statuses VALUES('f9c31d41-7e91-42a1-859b-e0991357566c', 'SENT') ON CONFLICT DO NOTHING;
INSERT INTO notif_statuses VALUES('303d5102-7595-4ba4-9c67-0c5d969d6a3e', 'READ') ON CONFLICT DO NOTHING;