DROP TABLE IF EXISTS notif_receivers;
DROP TABLE IF EXISTS notif_statuses;
DROP TABLE IF EXISTS notif_types;
DROP TABLE IF EXISTS notifications;

DROP TABLE IF EXISTS email_send_email;
DROP TABLE IF EXISTS email_text;