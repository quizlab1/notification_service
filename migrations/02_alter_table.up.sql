-- delete statements for notif_types
DELETE FROM notif_types WHERE id = '086032bc-fae4-4a85-aedd-83e37b52f309' AND name = 'FOLLOW';
DELETE FROM notif_types WHERE id = '23f0ed87-6874-40be-993a-140fa31d7050' AND name = 'LIKE';
DELETE FROM notif_types WHERE id = '70e21be4-d511-4284-b8e2-f1d94205ef1e' AND name = 'SYSTEM';

-- insert default values to notif_statuses table
DELETE FROM notif_statuses WHERE id = '2cb46139-7c5f-4e0d-88f6-50fe79986fa6' AND name = 'NEW';
DELETE FROM notif_statuses WHERE id = 'f9c31d41-7e91-42a1-859b-e0991357566c' AND name = 'SENT';
DELETE FROM notif_statuses WHERE id = '303d5102-7595-4ba4-9c67-0c5d969d6a3e' AND name = 'READ';

-- removing foreign keys from notif_receivers
ALTER TABLE notif_receivers DROP COLUMN IF EXISTS notif_type_id;
ALTER TABLE notif_receivers DROP COLUMN IF EXISTS notif_status_id;

-- notif_statuses table alter statements
ALTER TABLE notif_statuses DROP COLUMN IF EXISTS id;
ALTER TABLE notif_statuses DROP COLUMN IF EXISTS name;

DO $$
BEGIN
    ALTER TABLE notif_statuses ADD COLUMN notif_status VARCHAR(20);
EXCEPTION
    WHEN duplicate_column THEN
        RAISE NOTICE 'Field already exists. Skipping...';
END $$;

ALTER TABLE notif_statuses ADD CONSTRAINT "ck_notif_status" CHECK (notif_status=lower(notif_status));
ALTER TABLE notif_statuses ADD CONSTRAINT "pk_notif_status" PRIMARY KEY(notif_status);

-- notif_types table alter statements
ALTER TABLE notif_types DROP COLUMN IF EXISTS id;
ALTER TABLE notif_types DROP COLUMN IF EXISTS name;

DO $$
BEGIN
    ALTER TABLE notif_types ADD COLUMN notif_type VARCHAR(20);
EXCEPTION
    WHEN duplicate_column THEN
        RAISE NOTICE 'Field already exists. Skipping...';
END $$;

DO $$
BEGIN
    ALTER TABLE notif_types ADD CONSTRAINT "ck_notif_type" CHECK (notif_type=lower(notif_type));
EXCEPTION
    WHEN syntax_error THEN
        RAISE NOTICE 'Field already exists. Skipping...';
END $$;

ALTER TABLE notif_types ADD CONSTRAINT "pk_notif_type" PRIMARY KEY(notif_type);

-- notif_receivers alter statements
ALTER TABLE notif_receivers ADD COLUMN notif_type VARCHAR(20) NOT NULL REFERENCES notif_types(notif_type);
ALTER TABLE notif_receivers ADD COLUMN notif_status VARCHAR(20) NOT NULL REFERENCES notif_statuses(notif_status);

-- insert default types to notif_types table
INSERT INTO notif_types VALUES('follow') ON CONFLICT DO NOTHING;
INSERT INTO notif_types VALUES('like') ON CONFLICT DO NOTHING;
INSERT INTO notif_types VALUES('system') ON CONFLICT DO NOTHING;


-- insert default values to notif_statuses table
INSERT INTO notif_statuses VALUES('new') ON CONFLICT DO NOTHING;
INSERT INTO notif_statuses VALUES('sent') ON CONFLICT DO NOTHING;
INSERT INTO notif_statuses VALUES('read') ON CONFLICT DO NOTHING;