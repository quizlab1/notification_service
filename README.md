# Facetab Notification service documentation

### Prerequisites:
- `go` with version above **1.14**
- `make` binary package
- `docker.io` (recommended)

To run the project, first you need to create `.env` file by running the following command:

    make create-env

Customize `.env` file according to your needs by replacing values for environment variables. Then run command:

    set -a &&. ./.env && set +a

To run the project in _development mode_:

    $ go run cmd/main.go

    or

    $ make run-dev

To compile and build the project run:

    $ go build -mod=vendor -o ./bin/notification_service ./cmd/main.go

    or

    $ make build

To generate protos:

    make proto-gen

To pull recent changes in protos submodule:

    make update-proto-module

To run SQL migrations locally:

    make migrate-jeyran

To build docker image:

    make build-image

To push it run the following command (you need to login to docker registry first):

    make push-image

For running tests locally, linting your code, unit testing, race detection and memory sanitizing:

    make lint
    
    make unit-tests

    make race

    make msan

To delete unnecessary branches locally, run:

    make delete-branches