package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/quizlab/notification_service/storage/postgres"
	"gitlab.com/quizlab/notification_service/storage/repo"
)

// I contains methods that needs to be implemented
type I interface {
	Notification() repo.NotificationStorageI
	Email() repo.EmailStorageI
}

// NewStoragePg is a constructor for I
func NewStoragePg(db *sqlx.DB) I {
	return &storagePg{
		db:               db,
		notificationRepo: postgres.NewNotificationRepo(db),
		emailRepo:        postgres.NewEmailRepo(db),
	}
}

type storagePg struct {
	db               *sqlx.DB
	notificationRepo repo.NotificationStorageI
	emailRepo        repo.EmailStorageI
}

func (s storagePg) Notification() repo.NotificationStorageI {
	return s.notificationRepo
}

func (s storagePg) Email() repo.EmailStorageI {
	return s.emailRepo
}
