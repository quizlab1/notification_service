package repo

import (
	"errors"

	pb "gitlab.com/quizlab/notification_service/genproto/notification_service"
	pbu "gitlab.com/quizlab/notification_service/genproto/user_service"
)

var (
	// ErrInvalidField ...
	ErrInvalidField = errors.New("Incorrect field")
)

// NotificationStorageI interface
type NotificationStorageI interface {
	Create(notification *pb.Notification) error
	ChangeStatus(notificationID, receiverID, statusID string) error
	GetByStatus(statusID string) ([]*pb.NotificationReceivers, error)
	GetAllUserNotifications(receiverID string, page, limit uint64) ([]*pb.Notification, error)
	Delete(senderID string, receiverID string, notifType string, objectID string) error
	GetUserSentNotifsCount(receiverID string) (count string, err error)
	ChangeNotifStatuses(id string) (err error)
	SendNotifToUsers(noteID string, clients []*pbu.User, notifType string) (err error)
	SaveNotification(senderID string, content string) (notificationID string, err error)
	DeleteByObjectID(objectID string) (err error)

	// GetHomeEventInvites(userID string) (*pb.GetHomeEventInvitesResponse, error)
	DeleteByObjectIds(ids []string) (count int64, err error)
	DeleteNotifUserRefs(ids []string) (nCount, nrCount int64, err error)

	// Settings
	NotificationSettings(userID string) (*pb.NotificationSettingsResponse, error)
	NotificationSettingsUpdate(settings *pb.Settings) error
	CreateUserNotificationSettings(userID string) error
	DeleteUserNotificationSettings(userID string) error
	CheckNotificationSettingsStatus(userID, field string) (bool, error)
}
