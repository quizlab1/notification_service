package repo

// Email ...
type Email struct {
	ID             string
	Subject        string
	Body           string
	RecipientEmail string
}

// EmailStorageI ...
type EmailStorageI interface {
	GetNotSent() ([]Email, error)
	MakeSent(ID string) error
	Send(subject, body string, recipients []string) error
}

