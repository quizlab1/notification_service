package postgres

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/quizlab/notification_service/storage/repo"
)

type emailRepo struct {
	db *sqlx.DB
}

// NewEmailRepo ...
func NewEmailRepo(db *sqlx.DB) repo.EmailStorageI {
	return &emailRepo{db: db}
}

//GetNotSent ...
func (r *emailRepo) GetNotSent() ([]repo.Email, error) {
	var emails []repo.Email
	var query = `SELECT e.id, 
						e.email, 
						t.subject, 
						t.body 
				FROM email_text t 
				JOIN email_send_email e ON e.text_id=t.id 
				WHERE e.send_status=false`

	rows, err := r.db.Queryx(query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var email repo.Email
		if err := rows.Scan(
			&email.ID,
			&email.RecipientEmail,
			&email.Subject,
			&email.Body,
		); err != nil {
			return nil, err
		}
		emails = append(emails, email)
	}
	return emails, err
}

// MakeSent ...
func (r *emailRepo) MakeSent(ID string) error {
	var query = `UPDATE email_send_email SET send_status=true WHERE id=$1`
	_, err := r.db.Exec(query, ID)

	return err
}

func (r *emailRepo) Send(subject, body string, recipients []string) error {
	fmt.Print("send email postgres ...")
	textID, err := uuid.NewRandom()
	if err != nil {
		return err
	}
	insert := `
	INSERT INTO
	email_text
	(
		id,
		subject,
		body
	)
	values($1, $2, $3)
	`
	_, err = r.db.Exec(insert, textID, subject, body)
	if err != nil {
		return err
	}

	for _, email := range recipients {
		sendID, err := uuid.NewRandom()
		if err != nil {
			return err
		}
		insertSend := `
		INSERT INTO
		email_send_email
		(
			id,
			email,
			text_id
		)
		values($1, $2, $3)
		`
		_, err = r.db.Exec(insertSend, sendID, email, textID)
		if err != nil {
			return err
		}
	}
	return nil
}

