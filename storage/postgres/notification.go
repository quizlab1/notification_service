package postgres

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/quizlab/notification_service/config"
	pb "gitlab.com/quizlab/notification_service/genproto/notification_service"
	pbu "gitlab.com/quizlab/notification_service/genproto/user_service"
	"gitlab.com/quizlab/notification_service/storage/repo"
)

type notificationRepo struct {
	db *sqlx.DB
}

// NewNotificationRepo constructor
func NewNotificationRepo(db *sqlx.DB) repo.NotificationStorageI {
	return &notificationRepo{db: db}
}

func (r *notificationRepo) Create(notification *pb.Notification) error {
	var (
		objectID   sql.NullString
		objectType sql.NullString
	)

	noteID, err := uuid.NewRandom()
	if err != nil {
		return err
	}

	if notification.GetObjectId() != "" {
		objectID = sql.NullString{String: notification.ObjectId, Valid: true}
		objectType = sql.NullString{String: notification.ObjectType, Valid: true}
	}

	// initialize transaction
	tx, err := r.db.Beginx()
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			// rollback transaction before return in case of error
			_ = tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	// insert notification
	var query =`INSERT INTO notifications(
                          id, 
                          content, 
                          sender_id, 
                          object_id, 
                          object_type) 
				VALUES ($1, $2, $3, $4, $5)`
	_, err = tx.Exec(query,
		noteID,
		notification.Content,
		notification.SenderId,
		objectID,
		objectType)

	if err != nil {
		// TODO return custom error according to constraints
		return err
	}

	for _, rec := range notification.Receivers {
		var settingType string

		if rec.ReceiverId != notification.SenderId {
			if rec.Type == config.LikeType || rec.Type == config.CommentType {
				settingType = config.PostType
			} else if rec.Type == config.FollowType {
				settingType = config.FollowType
			}

			settingStatus, err := r.CheckNotificationSettingsStatus(rec.ReceiverId, settingType)
			if err != nil {
				return err
			}

			if settingStatus {
				id, err := uuid.NewRandom()
				if err != nil {
					return err
				}

				query = `INSERT INTO notif_receivers(
                            id, 
                            notif_id, 
                            receiver_id, 
                            notif_type, 
                            notif_status) 
						 VALUES ($1, $2, $3, $4, $5)`
				_, err = tx.Exec(query, id, noteID, rec.ReceiverId, rec.Type, config.NewStatus)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (r *notificationRepo) GetByStatus(status string) ([]*pb.NotificationReceivers, error) {
	var receivers []*pb.NotificationReceivers

	var query = `SELECT nr.notif_id, 
						n.content, 
						nr.receiver_id, 
						n.sender_id, 
						n.object_id, 
						n.object_type, 
						nr.notif_type
				FROM notif_receivers nr 
				JOIN notifications n on nr.notif_id = n.id 
				WHERE nr.notif_status=$1 
				ORDER BY created_at`
	rows, err := r.db.Queryx(query, status)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			receiver   pb.NotificationReceivers
			objectID   sql.NullString
			objectType sql.NullString
			notifType  string
		)

		err = rows.Scan(
			&receiver.NotificationId,
			&receiver.Content,
			&receiver.ReceiverId,
			&receiver.SenderId,
			&objectID,
			&objectType,
			&notifType)

		if err != nil {
			return nil, err
		}

		receiver.ObjectId = objectID.String
		receiver.ObjectType = objectType.String
		receiver.Type = notifType

		receivers = append(receivers, &receiver)
	}

	return receivers, nil
}

// GetAllNotifications selects all notifications from the database
func (r *notificationRepo) GetAllUserNotifications(receiverID string, page, limit uint64) (notifications []*pb.Notification, err error) {
	notifications = []*pb.Notification{}
	var (
		st     time.Time
		offset uint64
	)

	offset = (page - 1) * limit

	var query = `SELECT n.id, 
						n.content, 
						n.sender_id,
						nr.notif_status, 
						nr.notif_type, 
						nr.send_at,
						n.object_id,
						n.object_type
				FROM notifications n
				JOIN notif_receivers nr ON n.id = nr.notif_id
				WHERE nr.receiver_id = $1 
					AND nr.send_at IS NOT NULL
				ORDER BY nr.send_at DESC 
				LIMIT $2 OFFSET $3`

	rows, err := r.db.Query(query, receiverID, limit, offset)
	if err != nil {
		return
	}

	for rows.Next() {
		notification := pb.Notification{}
		err = rows.Scan(
			&notification.Id,
			&notification.Content,
			&notification.SenderId,
			&notification.Status,
			&notification.Type,
			&st,
			&notification.ObjectId,
			&notification.ObjectType,
		)
		if err != nil {
			return
		}

		notification.SentDate = st.Format(time.RFC3339)
		notifications = append(notifications, &notification)
	}
	return
}

func (r *notificationRepo) DeleteNotifUserRefs(ids []string) (nCount, nrCount int64, err error) {
	nCount, nrCount = 0, 0

	// initialize transaction
	tx, err := r.db.Beginx()
	if err != nil {
		return 0, 0, err
	}

	defer func() {
		if err != nil {
			// rollback transaction before return in case of error
			_ = tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	for _, id := range ids {
		result, err := tx.Exec(`DELETE FROM notifications WHERE sender_id = $1`, id)
		if err != nil {
			return 0, 0, err
		}

		if c, err := result.RowsAffected(); err != nil {
			return 0, 0, err
		} else if c != 0 {
			nCount += c
		}
	}

	for _, id := range ids {
		result, err := tx.Exec(`DELETE FROM notif_receivers WHERE receiver_id = $1`, id)
		if err != nil {
			return 0, 0, err
		}

		if c, err := result.RowsAffected(); err != nil {
			return 0, 0, err
		} else if c != 0 {
			nrCount += c
		}
	}

	return
}

func (r *notificationRepo) ChangeStatus(notificationID, receiverID, status string) error {
	_, err := r.db.Exec("UPDATE notif_receivers SET notif_status=$1, send_at=current_timestamp WHERE notif_id=$2", status, notificationID)

	if err != nil {
		return err
	}

	return nil
}

// Delete func varies depending on notifType.
func (r *notificationRepo) Delete(senderID string, receiverID string, notifType string, objectID string) error {
	var (
		err     error
		rows    *sql.Rows
		notifID string
	)

	if notifType == config.FollowType {
		// in the query bellow we pass 3 parameters if notif_type is follow
		// because one user can be followed by another user only one time.
		// LIMIT 1 just prevents returning multiple values instead of one
		query := `SELECT n.id 
						FROM notifications n 
						JOIN notif_receivers nr ON n.id = nr.notif_id
					WHERE n.sender_id = $1 
			  			AND nr.receiver_id = $2 
			  			AND nr.notif_type = $3`
		rows, err = r.db.Query(query, senderID, receiverID, notifType)
		if err != nil {
			return err
		}
		defer rows.Close()

	} else if notifType == config.LikeType || notifType == config.JoinType {
		// bellow we pass 4 parameters because one user can like
		// multiple posts or events of other users
		query := `SELECT n.id 
					FROM notifications n 
					JOIN notif_receivers nr ON n.id = nr.notif_id
				  WHERE n.sender_id = $1 
					AND nr.receiver_id = $2 
					AND nr.notif_type = $3
					AND n.object_id = $4`
		rows, err = r.db.Query(query, senderID, receiverID, notifType, objectID)
		if err != nil {
			return err
		}
		defer rows.Close()

	} else if notifType == config.JoinType {
		query := `SELECT n.id 
					FROM notifications n 
					JOIN notif_receivers nr ON n.id = nr.notif_id
				  WHERE n.sender_id = $1 
					AND nr.receiver_id = $2 
					AND nr.notif_type = $3
					AND n.object_id = $4`
		rows, err = r.db.Query(query, senderID, receiverID, notifType, objectID)
		if err != nil {
			return err
		}
		defer rows.Close()
	} else if notifType == config.UnfollowType {
		// in the query bellow we pass 3 parameters if notif_type is follow
		// because one user can be followed by another user only one time.
		// LIMIT 1 just prevents returning multiple values instead of one
		query := `SELECT n.id 
					FROM notifications n 
					JOIN notif_receivers nr ON n.id = nr.notif_id
				WHERE n.sender_id = $1 
					AND nr.receiver_id = $2 
					AND nr.notif_type = $3`
		rows, err = r.db.Query(query, senderID, receiverID, notifType)
		if err != nil {
			return err
		}
		defer rows.Close()

	} else {
		return errors.New("Delete for this notif_type is not implemented yet")
	}

	for rows.Next() {
		err = rows.Scan(&notifID)
		if err != nil {
			return err
		}

		_, err = r.db.Exec(`DELETE FROM notifications WHERE id = $1`, notifID)
		if err != nil {
			return err
		}
	}

	return nil
}

// GetUserSentNotifsCount method
func (r *notificationRepo) GetUserSentNotifsCount(receiverID string) (count string, err error) {
	query := `SELECT count(*) 
				FROM notif_receivers 
			WHERE receiver_id = $1
				AND (notif_status = 'sent' 
				OR notif_status = 'new')`
	err = r.db.QueryRow(query, receiverID).Scan(&count)
	return
}

// ChangeNotifStatuses accepts array of ids and changes its statuses to read
func (r *notificationRepo) ChangeNotifStatuses(id string) (err error) {
	_, err = r.db.Exec(`UPDATE notif_receivers SET notif_status = 'read' WHERE receiver_id = $1`, id)
	if err != nil {
		return
	}
	return
}

func (r *notificationRepo) SaveNotification(senderID string, content string) (notificationID string, err error) {
	noteID, err := uuid.NewRandom()
	if err != nil {
		return
	}

	query := `INSERT INTO notifications(
				id,
				content,
				sender_id)
			VALUES ($1, $2, $3) RETURNING id`
	err = r.db.QueryRow(query, noteID, content, senderID).
		Scan(&notificationID)

	return
}

// SendNotifToAllUsers is used to send notification to all users
// senderID is a reference to
func (r *notificationRepo) SendNotifToUsers(noteID string, clients []*pbu.User, notifType string) (err error) {

	query := `INSERT INTO notif_receivers(
				id,
				notif_id,
				receiver_id,
				notif_type,
				notif_status) 
			  VALUES ($1, $2, $3, $4, $5)`

	for _, client := range clients {
		var id uuid.UUID
		id, err = uuid.NewRandom()
		if err != nil {
			return
		}

		_, err = r.db.Exec(query, id, noteID, client.GetId(), "client", notifType, config.NewStatus)
		if err != nil {
			return
		}
	}

	return
}

// DeleteByObjectID deletes notifications by post ID or event ID. It is useful when, for example
// a liked post has been deleted
func (r *notificationRepo) DeleteByObjectID(objectID string) (err error) {
	_, err = r.db.Exec(`DELETE FROM notifications WHERE object_id = $1`, objectID)

	return
}

func (r *notificationRepo) DeleteByObjectIds(ids []string) (count int64, err error) {
	count = 0

	// initialize transaction
	tx, err := r.db.Beginx()
	if err != nil {
		return 0,  err
	}

	defer func() {
		if err != nil {
			// rollback transaction before return in case of error
			_ = tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	for _, id := range ids {
		result, err := tx.Exec(`DELETE FROM notifications WHERE object_id = $1`, id)
		if err != nil {
			return 0, err
		}

		if c, err := result.RowsAffected(); err != nil {
			return 0, err
		} else if c != 0 {
			count += c
		}
	}

	return
}

func (r *notificationRepo) NotificationSettings(userID string) (*pb.NotificationSettingsResponse, error) {
	var (
		settings pb.NotificationSettingsResponse
	)

	query := `SELECT 
				post, 
				follow, 
				direct_message, 
				email
			FROM notif_settings 
			WHERE user_id=$1`
	row := r.db.QueryRow(query, userID)

	if err := row.Scan(
		&settings.Post,
		&settings.Follow,
		&settings.DirectMessage,
		&settings.Email,
	); err != nil {
		return nil, err
	}

	return &settings, nil
}

func (r *notificationRepo) NotificationSettingsUpdate(settings *pb.Settings) error {
	query := `UPDATE notif_settings 
				SET
					post=$1,
					follow=$2,
					direct_message=$3,
					email=$4
				WHERE user_id=$5`

	_, err := r.db.Exec(query,
		settings.Post,
		settings.Follow,
		settings.DirectMessage,
		settings.Email,
		settings.UserId,
	)
	if err != nil {
		return err
	}
	return nil
}

func (r *notificationRepo) DeleteUserNotificationSettings(id string) error {
	_, err := r.db.Exec(`DELETE FROM notif_settings WHERE user_id=$1`, id)
	if err != nil {
		return err
	}

	return nil
}

func (r *notificationRepo) CreateUserNotificationSettings(id string) (err error) {
	var query = "INSERT INTO notif_settings(user_id) VALUES($1)"
	_, err = r.db.Exec(query,id)

	return err
}

func (r *notificationRepo) CheckNotificationSettingsStatus(userID, field string) (result bool, err error) {
	switch field {
	case "post", "follow", "direct_message", "email":
		var query = fmt.Sprintf("SELECT %s FROM notif_settings WHERE user_id=$1", field)
		err = r.db.QueryRow(query, userID).Scan(&result)
	default:
		err = repo.ErrInvalidField
	}

	return result, err
}
