package models

import (
	"fmt"

	"github.com/go-playground/validator"
)

var (
	validate = validator.New()
)

// SendSMSData ...
type SendSMSData struct {
	PhoneNumber string `validate:"e164,required"`
}

// Validate ...
func (e *SendSMSData) Validate() (err error) {
	err = validate.Struct(e)
	if err != nil {
		for _, er := range err.(validator.ValidationErrors) {
			if er.Tag() == "required" {
				return fmt.Errorf("%s field cannot be blank", er.Field())
			}
			if er.Tag() == "e164" {
				return fmt.Errorf("Invalid phone number")
			}
		}
	}

	return nil
}
