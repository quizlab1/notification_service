package service

import (
	"context"
	"fmt"
	"strings"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"github.com/sfreiberg/gotwilio"
	"gitlab.com/quizlab/notification_service/config"
	gpb "github.com/golang/protobuf/ptypes/empty"
	pb "gitlab.com/quizlab/notification_service/genproto/notification_service"
	pbu "gitlab.com/quizlab/notification_service/genproto/user_service"
	"gitlab.com/quizlab/notification_service/pkg/logger"
	"gitlab.com/quizlab/notification_service/service/grpcclient"
	"gitlab.com/quizlab/notification_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// NotificationService struct
type NotificationService struct {
	storage storage.I
	logger  logger.Logger
	client  *grpcclient.GrpcClient
	twilio  *gotwilio.Twilio
	config  config.Config
}

// NewNotificationService constructor
func NewNotificationService(db *sqlx.DB, log logger.Logger, client *grpcclient.GrpcClient, twilio *gotwilio.Twilio, config config.Config) *NotificationService {
	return &NotificationService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		client:  client,
		twilio:  twilio,
		config:  config,
	}
}

// Create method
func (ns *NotificationService) Create(ctx context.Context, req *pb.Notification) (*empty.Empty, error) {
	
	err := ns.storage.Notification().Create(req)
	if err != nil {
		ns.logger.Error("Error while creating notification", logger.Error(err))

		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &empty.Empty{}, nil
}

// GetByStatusID method
func (ns *NotificationService) GetByStatusID(statusID string) ([]*pb.NotificationReceivers, error) {
	receivers, err := ns.storage.Notification().GetByStatus(statusID)
	if err != nil {
		// raven.ReportIfError(err)
		// ns.logger.Error("Error while getting not sending users", logger.Error(err))

		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return receivers, nil
}

// GetCount method is accessed from deamons daemon
func (ns *NotificationService) GetCount(receiverID string) (string, error) {
	count, err := ns.storage.Notification().GetUserSentNotifsCount(receiverID)
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("Error while getting count of user notifications", logger.Error(err))

		return "", status.Error(codes.Internal, "Internal server error")
	}

	return count, nil
}

// DeleteNotifUserRefs ...
func (ns *NotificationService) DeleteNotifUserRefs(ctx context.Context, in *pb.NotifUserRefsReq) (*pb.NotifUserRefsResp, error) {
	nCount, nrCount, err := ns.storage.Notification().DeleteNotifUserRefs(in.UserIds)
	if err != nil {
		ns.logger.Error("Error while deleting notif user refs", logger.Error(err))
		// raven.ReportIfError(err)
		return nil, status.Error(codes.Internal, "Internal Server Error")
	}

	if nCount != 0 {
		_ = fmt.Sprintf("Successfully deleted %d notifications", nCount)
		// raven.ReportMessage(msg)
	}

	if nrCount != 0 {
		_ = fmt.Sprintf("Successfully deleted %d notif_receivers", nrCount)
		// raven.ReportMessage(msg)
	}

	return &pb.NotifUserRefsResp{RowsAffected: nCount + nrCount}, nil
}

// ChangeStatus method
func (ns *NotificationService) ChangeStatus(ctx context.Context, req *pb.ChangeStatusRequest) (*empty.Empty, error) {
	err := ns.storage.Notification().ChangeStatus(req.NotificationId, req.ReceiverId, req.Status)
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("error while updating notification status id", logger.Error(err))

		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &empty.Empty{}, nil
}

// GetAllUserNotifications method
func (ns *NotificationService) GetAllUserNotifications(ctx context.Context, in *pb.GetAllUserNotificationsRequest) (*pb.GetAllUserNotificationsResponse, error) {
	notifications, err := ns.storage.Notification().GetAllUserNotifications(in.GetReceiverId(), in.GetPage(), in.GetLimit())
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("error while getting user notifications by id", logger.Error(err))

		return nil, status.Error(codes.Internal, "Internal server error")
	}

	for _, note := range notifications {
		user, err := ns.client.UserService().GetAuthUser(ctx, &pbu.GetAuthUserReq{Id: note.SenderId})
		if err != nil {
			if strings.Contains(err.Error(), "Not found") {
				_ = ns.storage.Notification().Delete(note.SenderId, in.ReceiverId, note.Type, "")
			} else {
				// raven.ReportIfError(err)
				ns.logger.Error("error while getting client in GetAllUserNotifications service", logger.Error(err))

				return nil, err
			}
		}

		if user != nil {
			note.Username = user.Username
			note.Name = user.FirstName + " " + user.LastName
			note.FirstName = user.FirstName
		}
		_, err = ns.client.UserService().IsUserFollowing(
			ctx, &pbu.IsUserFollowingRequest{
				FollowerId:  in.GetReceiverId(),
				FollowingId: note.GetSenderId(),
			},
		)
		if err != nil {
			// raven.ReportIfError(err)
			ns.logger.Error("error while getting all user notifications is_following boolean", logger.Error(err))
		}
		// note.IsFollowing = res.GetIsFollowing()

		// if user.GetProfilePhoto() != "" && !strings.Contains(user.GetProfilePhoto(), "http") {
		// 	user.ProfilePhoto = &wrappers.StringValue{
		// 		Value: md.GetAvatars300x300Url(user.ProfilePhoto.GetValue()),
		// 	}
		// }
		note.ProfilePhoto = user.GetProfilePhoto()

	}

	return &pb.GetAllUserNotificationsResponse{Notifications: notifications}, nil
}

// Delete method for notifications
func (ns *NotificationService) Delete(ctx context.Context, in *pb.DeleteNotifRequest) (*empty.Empty, error) {
	err := ns.storage.Notification().Delete(in.SenderId, in.ReceiverId, in.NotifType, in.ObjectId)
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("Error while deleting notification", logger.Error(err))

		return nil, err
	}

	return &empty.Empty{}, nil
}

// GetUserSentNotifsCount ...
func (ns *NotificationService) GetUserSentNotifsCount(ctx context.Context, in *pb.GetAllUserNotificationsRequest) (*pb.GetUserSentNotifsCountResponse, error) {
	count, err := ns.storage.Notification().GetUserSentNotifsCount(in.GetReceiverId())
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("Error while getting notification counts", logger.Error(err))

		return nil, err
	}

	return &pb.GetUserSentNotifsCountResponse{Count: count}, nil
}

// ChangeNotifStatuses ...
func (ns *NotificationService) ChangeNotifStatuses(ctx context.Context, in *pb.ChangeNotifStatusesRequest) (*empty.Empty, error) {
	err := ns.storage.Notification().ChangeNotifStatuses(in.ReceiverId)
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("Error while getting notification counts", logger.Error(err))

		return nil, err
	}

	return &empty.Empty{}, nil
}

// SendNotifToAllUsers service method
func (ns *NotificationService) SendNotifToAllUsers(ctx context.Context, in *pb.SendNotificationRequest) (*empty.Empty, error) {
	var (
		limit uint64 = 1000
		page  uint64 = 1
	)
	noteID, err := ns.storage.Notification().SaveNotification(in.GetSenderID(), in.GetContent())
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("error while saving notification to db", logger.Error(err))

		return nil, err
	}

	for {
		users, err := ns.client.UserService().GetAllAuthUsers(ctx, &pbu.GetAllAuthUsersReq{
			Limit: limit,
			Page:  page,
		})
		if err != nil {
			// raven.ReportIfError(err)
			ns.logger.Error("Error while getting users in SendNotifToAllUsers method", logger.Error(err))

			return nil, err
		}

		err = ns.storage.Notification().SendNotifToUsers(
			noteID, users.Users, in.NotifType,
		)
		if err != nil {
			// raven.ReportIfError(err)
			ns.logger.Error("Error while sending notification to all clients", logger.Error(err))

			return nil, err
		}

		if users.Size() == 0 {
			break
		}
		page++
	}

	return &empty.Empty{}, nil
}

// DeleteByObjectID is a service for deleting notification by objectID
func (ns *NotificationService) DeleteByObjectID(ctx context.Context, in *pb.DeleteNotifRequest) (*empty.Empty, error) {
	err := ns.storage.Notification().DeleteByObjectID(in.ObjectId)
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("Error while deleting notification by objectID", logger.Error(err))

		return nil, err
	}

	return &empty.Empty{}, err
}

// SendNotifToSpecificUsers service method
func (ns *NotificationService) SendNotifToSpecificUsers(ctx context.Context, in *pb.SendNotificationRequest) (*empty.Empty, error) {
	noteID, err := ns.storage.Notification().SaveNotification(in.GetSenderID(), in.GetContent())
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("error while saving notification to db", logger.Error(err))

		return nil, err
	}

	clients := []*pbu.User{}
	for _, id := range in.ReceiverIds {
		client := &pbu.User{}
		client, err = ns.client.UserService().GetAuthUser(ctx, &pbu.GetAuthUserReq{
			Id: id,
		})
		if err != nil {
			// raven.ReportIfError(err)
			ns.logger.Error("error while getting specified clients while sending notification", logger.Error(err))

			return nil, err
		}

		clients = append(clients, client)
	}

	err = ns.storage.Notification().SendNotifToUsers(noteID, clients, in.NotifType)
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("Error while sending notification to specific clients", logger.Error(err))

		return nil, err
	}

	return &empty.Empty{}, nil
}

// DeleteByObjectIds ...
func (ns *NotificationService) DeleteByObjectIds(ctx context.Context, in *pb.ObjectIdsRequest) (*pb.ObjectIdsResponse, error) {
	count, err := ns.storage.Notification().DeleteByObjectIds(in.ObjectIds)
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("Error while deleting object by ids (NotificationService)", logger.Error(err))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	if count != 0 {
		_ = fmt.Sprintf("Successfully deleted %d notifications", count)
		// raven.ReportMessage(msg)
	}

	return &pb.ObjectIdsResponse{RowsAffected: count}, nil
}

// NotificationSettings method
func (ns *NotificationService) NotificationSettings(ctx context.Context, req *pb.NotificationSettingsRequest) (*pb.NotificationSettingsResponse, error) {
	settings, err := ns.storage.Notification().NotificationSettings(req.GetUserId())
	if err != nil {
		// raven.ReportIfError(err)
		ns.logger.Error("Error while getting notification settings", logger.Error(err))

		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return settings, nil
}

// NotificationSettingsUpdate ...
func (ns *NotificationService) NotificationSettingsUpdate(ctx context.Context, req *pb.Settings) (*gpb.Empty, error) {
	err := ns.storage.Notification().NotificationSettingsUpdate(req)
	if err != nil {
		ns.logger.Error("failed update notification settings", logger.Error(err))
		return &gpb.Empty{}, status.Error(codes.Internal, "failed to notification settings")
	}

	return &gpb.Empty{}, nil
}

// DeleteUserNotificationSettings ...
func (ns *NotificationService) DeleteUserNotificationSettings(ctx context.Context, req *pb.DeleteUserNotificationSettingsRequest) (*gpb.Empty, error) {
	err := ns.storage.Notification().DeleteUserNotificationSettings(req.GetUserId())
	if err != nil {
		ns.logger.Error("failed delete user notification settings", logger.Error(err))
		return &gpb.Empty{}, status.Error(codes.Internal, "failed to delete user notification settings")
	}

	return &gpb.Empty{}, nil
}

// CreateUserNotificationSettings ...
func (ns *NotificationService) CreateUserNotificationSettings(ctx context.Context, req *pb.CreateUserNotificationSettingRequest) (*gpb.Empty, error) {
	err := ns.storage.Notification().CreateUserNotificationSettings(req.GetUserId())
	if err != nil {
		ns.logger.Error("failed create user notification settings", logger.Error(err))
		return &gpb.Empty{}, status.Error(codes.Internal, "failed to create user notification settings")
	}

	return &gpb.Empty{}, nil
}