package service

import (
	"context"
	"fmt"

	gpb "github.com/golang/protobuf/ptypes/empty"
	pb "gitlab.com/quizlab/notification_service/genproto/notification_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

//SendEmail ...
func (s *NotificationService) SendEmail(ctx context.Context, req *pb.SendEmailRequest) (*gpb.Empty, error) {
	fmt.Print("send service ...")
	err := s.storage.Email().Send(req.Subject, req.Body, req.Recipients)
	if err != nil {
		return &gpb.Empty{}, status.Error(codes.Internal, "Internal server error")
	}
	return &gpb.Empty{}, nil
}

