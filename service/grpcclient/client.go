package grpcclient

import (
	"fmt"

	"gitlab.com/quizlab/notification_service/config"
	pb "gitlab.com/quizlab/notification_service/genproto/user_service"
	"google.golang.org/grpc"
)

// I is an interface for grpc client
type I interface {
	UserService() pb.UserServiceClient
}

// GrpcClient ...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

// New ...
func New(cfg config.Config) (*GrpcClient, error) {
	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.UserServiceHost, cfg.UserServicePort),
		grpc.WithInsecure())

	if err != nil {
		return nil, fmt.Errorf("user service dial host: %s port: %d",
			cfg.UserServiceHost, cfg.UserServicePort)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service": pb.NewUserServiceClient(connUser),
		},
	}, nil
}

//UserService ...
func (g *GrpcClient) UserService() pb.UserServiceClient {
	return g.connections["user_service"].(pb.UserServiceClient)
}