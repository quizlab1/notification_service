package service

import (
	"context"

	"gitlab.com/quizlab/notification_service/config"
	pb "gitlab.com/quizlab/notification_service/genproto/notification_service"
	"gitlab.com/quizlab/notification_service/service/models"
	"gitlab.com/quizlab/notification_service/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	l "gitlab.com/quizlab/notification_service/pkg/logger"
)

// SendSMS method
func (ns *NotificationService) SendSMS(ctx context.Context, req *pb.SendSMSRequest) (*pb.SendSMSResponse, error) {
	validateData := models.SendSMSData{
		PhoneNumber: req.GetPhoneNumber(),
	}

	err := validateData.Validate()
	if err != nil {
		ns.logger.Error("failed to send sms, invalid request", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	code := utils.GenerateCode(5)
	ns.twilio.SendSMS(config.TwilioFrom, req.PhoneNumber, code, "", "")

	return &pb.SendSMSResponse{Code: code}, nil
}
