package deamons

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/quizlab/notification_service/pkg/logger"
	"gitlab.com/quizlab/notification_service/storage"
	"go.uber.org/zap"

	"gitlab.com/quizlab/notification_service/config"
	rp "gitlab.com/quizlab/notification_service/storage/repo"
	gomail "gopkg.in/gomail.v2"

	"time"
)

// EmailDaemon ...
type EmailDaemon struct {
	conf    config.Config
	storage storage.I
	log     logger.Logger
}

// NewEmailDaemon ...
func NewEmailDaemon(db *sqlx.DB, log logger.Logger, config config.Config) *EmailDaemon {
	return &EmailDaemon{
		storage: storage.NewStoragePg(db),
		log:     log,
		conf:    config,
	}
}

// Init ...
func (d *EmailDaemon) Init() {
	c := make(chan string)

	isShuttingDown := false
	started := false

OUTTER:
	for {
		if isShuttingDown {
			break
		}
		select {
		case s, ok := <-c:

			if ok {
				d.log.Info("received signal", zap.String("signal", s))
				d.log.Info("email daemon is going down...")
				isShuttingDown = true
				c = nil
				continue OUTTER
			}
		default:
			var emails []rp.Email
			emails, err := d.storage.Email().GetNotSent()
			if err != nil {
				d.log.Error("error while checking email status ", zap.Error(err))
			}
			for _, email := range emails {
				err := d.sendEmail(email.Subject, email.Body, email.RecipientEmail)

				if err != nil {
					d.log.Error("error while sending email")
				} else {
					err = d.storage.Email().MakeSent(email.ID)

					if err != nil {
						d.log.Error("error while updating email status")
					}
				}
			}

			if !started {
				started = true
			}
			//t := time.Now()
			//fmt.Println(fmt.Sprint("Service is running...\n", t.Format(time.RFC3339)))
			time.Sleep(15000 * time.Millisecond)
		}
	}
	d.log.Info("email daemon has stopped")
}

// Send Mail...
func (d *EmailDaemon) sendEmail(subject, body, email string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", d.conf.Email.EmailFromHeader)
	m.SetHeader("To", email)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	// Send the email to
	dialer := gomail.NewPlainDialer(d.conf.Email.SMTPHost, d.conf.Email.SMTPPort, d.conf.Email.SMTPUser, d.conf.Email.SMTPUserPass)
	if err := dialer.DialAndSend(m); err != nil {
		panic(err)
	}
	d.log.Info("email successfully sent")
	return nil
}
