package deamons

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/appleboy/go-fcm"
	"gitlab.com/quizlab/notification_service/config"
	pb "gitlab.com/quizlab/notification_service/genproto/notification_service"
	pbu "gitlab.com/quizlab/notification_service/genproto/user_service"
	"gitlab.com/quizlab/notification_service/pkg/logger"
	"gitlab.com/quizlab/notification_service/service"
	"gitlab.com/quizlab/notification_service/service/grpcclient"
)


// PushDaemon ...
type PushDaemon struct {
	logger logger.Logger
	config config.Config
	client *grpcclient.GrpcClient
	service *service.NotificationService
}


// NewPushDaemon ...
func NewPushDaemon(logger logger.Logger, config config.Config, client *grpcclient.GrpcClient, service *service.NotificationService) *PushDaemon {
	return &PushDaemon{
		logger: logger,
		config: config,
		client: client,
		service: service,
	}
}

// Init ...
func (daemon *PushDaemon) Init() {
	// ns := service.NewNotificationService(daemon.db, daemon.logger, daemon.client)
	client, err := fcm.NewClient(daemon.config.APIKey)
	ttl := uint(60 * 60 * 24)

	if err != nil {
		daemon.logger.Error("api key is invalid", logger.Error(err))
	}

	for {
		receivers, err := daemon.service.GetByStatusID(config.NewStatus)
		if err != nil {
			// daemon.logger.Error("Error while getting receivers who get notification", logger.Error(err))
			continue
		}

		for _, rec := range receivers {
			counts, err := daemon.service.GetCount(rec.GetReceiverId())
			if err != nil {
				daemon.logger.Error("cannot get count of notifications", logger.Error(err))
				continue
			}

			cl, err := daemon.client.UserService().GetAuthUser(
				context.Background(),
				&pbu.GetAuthUserReq{Id: rec.ReceiverId},
			)

			if err != nil {
				daemon.logger.Error("user not found, deleting all his notifications...", logger.Error(err), logger.Any("req", rec.ReceiverId))

				_, err = daemon.service.DeleteNotifUserRefs(context.Background(), &pb.NotifUserRefsReq{
					UserIds: []string{rec.ReceiverId},
				})
				if err != nil {
					daemon.logger.Error("Error while deleting notifications by user refs")
				}

				continue
			}

			if cl.FcmToken == "" {
				daemon.logger.Warn("user doesnt't have deamons token, updating status to sent anyway", logger.Any("user", cl.Id))

				_, err := daemon.service.ChangeStatus(context.Background(), &pb.ChangeStatusRequest{
					NotificationId: rec.NotificationId,
					ReceiverId:     rec.ReceiverId,
					Status:         config.SentStatus,
				})
				if err != nil {
					daemon.logger.Error("error notification status did not update", logger.Error(err))
				}

				continue
			}
			content, title, objectID, err := generateNotificationBody(rec, daemon)
			if err != nil {
				_, err = daemon.service.DeleteNotifUserRefs(context.Background(), &pb.NotifUserRefsReq{
					UserIds: []string{rec.ReceiverId},
				})
				if err != nil {
					daemon.logger.Error("Error while deleting notifications by user refs")
				}

				continue
			}

			notif := &fcm.Message{
				To: cl.FcmToken,
				Notification: &fcm.Notification{
					Title: title,
					Body:  content,
					Badge: counts,
				},
				Data: map[string]interface{}{"objectID":string(objectID)},
				TimeToLive: &ttl,
			}

			r, err := client.Send(notif)
			if err != nil {
				daemon.logger.Error("error while sending notifications", logger.Error(err))
				continue
			}

			if r.Success == 1 {
				daemon.logger.Info("notification sent")
				_, err := daemon.service.ChangeStatus(context.Background(), &pb.ChangeStatusRequest{
					NotificationId: rec.NotificationId,
					ReceiverId:     rec.ReceiverId,
					Status:         config.SentStatus,
				})

				if err != nil {
					daemon.logger.Error("error sending notification status did not update", logger.Error(err))
					continue
				}
			} else {
				
				daemon.logger.Info("notification did not send, updating status to sent anyway", logger.Any("error", r.Error), logger.String("client_id", cl.Id))

				_, err := daemon.service.ChangeStatus(context.Background(), &pb.ChangeStatusRequest{
					NotificationId: rec.NotificationId,
					ReceiverId:     rec.ReceiverId,
					Status:         config.SentStatus,
				})
				if err != nil {
					daemon.logger.Error("error: notification status did not update", logger.Error(err))
				}
			}

		}

		if os.Getenv("ENVIRONMENT") == "production" {
			<-time.After(200 * time.Nanosecond)
		} else {
			<-time.After(1 * time.Second)
		}
	}
}

func generateNotificationBody(rec *pb.NotificationReceivers, daemon *PushDaemon) (string, string, string, error) {
	var title string
	var name string
	var objectID string
	user, err := daemon.client.UserService().GetAuthUser(
		context.Background(),
		&pbu.GetAuthUserReq{
			Id: rec.SenderId,
		})

	if err != nil {
		daemon.logger.Error("error while getting sender info", logger.Error(err))
		return "", "", "", err
	}
	name = user.GetFirstName() + " " + user.GetLastName()
	title = name
	objectID = rec.ObjectId

	return fmt.Sprintf("%s %s", name, rec.Content), title, objectID, nil
}
